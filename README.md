## How to Start

Please run the following scripts to start:

### `yarn install` or `npm install`

The app was written with plain CSS and used almost no library to build the UI to reduce the bundle size

### `yarn start` or `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
