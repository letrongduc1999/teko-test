import React, { useEffect, useState, Fragment } from "react";
import "./App.css";
import { Product } from "./models/product";
import { Color } from "./models/color";
import { getColors, getProducts } from "./services/products";

import Button from "./components/Button";
import Errors from "./components/Errors";
import Pagination from "./components/Pagination";
import Modal from "./components/Modal";
import { DEFAULT_PAGE, ITEM_PER_PAGE } from "./constants";
import { getProductsErrorMessages } from "./helpers";
import TableHead from "./components/Table/TableHead";
import TableRow from "./components/Table/TableRow";

const columns: string[] = [
  "ID",
  "Error Description",
  "Product Image",
  "Product Name",
  "SKU",
  "Color",
];

function App() {
  const [products, setProducts] = useState<Product[]>();
  const [colors, setColors] = useState<Color[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [showErrors, setShowErrors] = useState<boolean>(false);
  const [activePage, setActivePage] = useState(DEFAULT_PAGE);
  const [itemPerPage, setItemPerPage] = useState(ITEM_PER_PAGE);

  useEffect(() => {
    setIsLoading(true);
    getProducts()
      .then((res) => {
        setProducts(res);
      })
      .finally(() => setIsLoading(false));
    getColors().then((res) => setColors(res));
  }, []);

  const errors = getProductsErrorMessages(products || []);

  const totalPage = !!products?.length
    ? Math.ceil(products.length / itemPerPage)
    : 1;

  return (
    <Fragment>
      {isOpen && (
        <Modal
          setOpen={setIsOpen}
          title="Re-uploaded Products"
          products={products || []}
          colors={colors || []}
        />
      )}
      <div className="container">
        <div className="header-flex">
          <div className="header-text">Jason - Re-upload Error Products</div>
          <div>
            <Button
              value="Submit"
              className="submit-button"
              onClick={() => {
                if (errors.length === 0) {
                  setIsOpen(true);
                  setShowErrors(false);
                } else {
                  setShowErrors(true);
                }
              }}
            />
          </div>
        </div>
        {showErrors && <Errors errors={errors} />}
        <table className="table">
          <TableHead columns={columns} />
          <tbody className="table-body">
            {isLoading && <Fragment>Loading...</Fragment>}
            {!isLoading &&
              !!products?.length &&
              products
                .filter(
                  (_, index) =>
                    index <= activePage * itemPerPage - 1 &&
                    index >= (activePage - 1) * itemPerPage
                )
                .map((product, index) => {
                  return (
                    <TableRow
                      key={index}
                      colors={colors}
                      product={product}
                      products={products}
                      setProducts={setProducts}
                    />
                  );
                })}
            {!isLoading && !products?.length && (
              <Fragment>Nothing to show</Fragment>
            )}
          </tbody>
        </table>
        <Pagination
          activePage={activePage}
          setActivePage={setActivePage}
          setItemPerPage={setItemPerPage}
          totalPage={totalPage}
          itemPerPage={itemPerPage}
        />
      </div>
    </Fragment>
  );
}

export default App;
