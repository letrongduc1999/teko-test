const IMAGE_PLACE_HOLDER = "https://via.placeholder.com/150";
const NAME_MAX_LENGTH = 50;
const SKU_MAX_LENGTH = 20;
const IS_NAME_REQUIRED = true;
const DEFAULT_PAGE = 1;
const ITEM_PER_PAGE = 10;
const ITEM_PER_PAGE_OPTIONS = [
  {
    id: 10,
    name: "10",
  },
  {
    id: 20,
    name: "20",
  },
  {
    id: 50,
    name: "50",
  },
];

const COLORS: Record<number, string> = {
  1: "White",
  2: "Black",
  3: "Red",
  4: "Green",
  5: "Blue",
  6: "Yellow",
};

export {
  IMAGE_PLACE_HOLDER,
  SKU_MAX_LENGTH,
  NAME_MAX_LENGTH,
  IS_NAME_REQUIRED,
  DEFAULT_PAGE,
  ITEM_PER_PAGE,
  ITEM_PER_PAGE_OPTIONS,
  COLORS,
};
