export interface Product {
  id: number;
  errorDescription: string;
  name?: string;
  sku?: string;
  image: string;
  color?: number
}