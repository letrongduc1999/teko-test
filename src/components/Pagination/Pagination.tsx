import React from "react";
import Button from "../Button";
import Select from "../Select";
import { ITEM_PER_PAGE_OPTIONS } from "../../constants";

interface PaginationProps {
  activePage: number;
  totalPage: number;
  itemPerPage: number;
  setActivePage: (activePage: number) => void;
  setItemPerPage: (itemPerPage: number) => void;
}

const Pagination: React.FC<PaginationProps> = (props) => {
  const { activePage, itemPerPage, totalPage, setActivePage, setItemPerPage } =
    props;
  return (
    <div className="pagination-section">
      <div>
        <label>Item Per Page : </label>
        <Select
          defaultValue={itemPerPage}
          options={ITEM_PER_PAGE_OPTIONS}
          onChange={(e) => {
            const newValue = Number(e.target.value);
            !isNaN(newValue) && setItemPerPage(Number(e.target.value));
            setActivePage(1);
          }}
        />
      </div>
      <div>
        <div className="pagination">
          <Button
            value="Prev"
            disabled={activePage === 1}
            onClick={() => setActivePage(activePage - 1)}
          />
          {[...Array(totalPage)].map((e, i) => (
            <div
              className={`pagination-item ${
                activePage === i + 1 && "item-active"
              }`}
              key={i}
              onClick={() => {
                setActivePage(i + 1);
              }}
            >
              {i + 1}
            </div>
          ))}
          <Button
            value="Next"
            disabled={activePage === totalPage}
            onClick={() => setActivePage(activePage + 1)}
          />
        </div>
      </div>
    </div>
  );
};

export default Pagination;
