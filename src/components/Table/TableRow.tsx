import React from "react";
import { Product } from "../../models/product";
import { Color } from "../../models/color";
import Input from "../../components/Input";
import Select from "../../components/Select";

import {
  IMAGE_PLACE_HOLDER,
  IS_NAME_REQUIRED,
  NAME_MAX_LENGTH,
  SKU_MAX_LENGTH,
} from "../../constants";

interface TableRowProps {
  product: Product;
  products: Product[];
  colors?: Color[];
  setProducts: (products: Product[]) => void;
}

const TableRow: React.FC<TableRowProps> = (props) => {
  const { product, products, colors, setProducts } = props;

  const {
    color,
    errorDescription,
    id: sourceId,
    image,
    name,
    sku: sourceSku,
  } = product;

  const displayImage = image || IMAGE_PLACE_HOLDER;

  return (
    <tr key={sourceId} className="table-row">
      <td key={`id_${sourceId}`}>{sourceId}</td>
      <td key={`errorDescription_${sourceId}`}>{errorDescription}</td>
      <td key={`img_${sourceId}`}>
        <img src={displayImage} alt={name} width={"150px"} />
      </td>
      <td key={`name_${sourceId}`}>
        <Input
          maximumLength={NAME_MAX_LENGTH}
          isRequired={IS_NAME_REQUIRED}
          defaultValue={name}
          onChange={(event) => {
            const newName = event.target.value.trim();
            setProducts(
              products.map(({ id, name, ...remainAttr }) => {
                return {
                  id,
                  ...remainAttr,
                  name: sourceId === id ? newName : name,
                };
              })
            );
          }}
        />
      </td>
      <td key={`sku_${sourceId}`}>
        <Input
          maximumLength={SKU_MAX_LENGTH}
          defaultValue={sourceSku}
          isRequired={IS_NAME_REQUIRED}
          onChange={(event) => {
            const newSku = event.target.value.trim();
            setProducts(
              products.map(({ id, sku, ...remainAttr }) => {
                return {
                  id,
                  ...remainAttr,
                  sku: sourceId === id ? newSku : sku,
                };
              })
            );
          }}
        />
      </td>
      <td key={`color_${sourceId}`}>
        <Select
          defaultValue={color}
          options={colors}
          onChange={(event) => {
            const newColor = Number(event.target.value);
            const isValidColor = !Number.isNaN(newColor);
            if (isValidColor) {
              setProducts(
                products.map(({ id, color, ...remainAttr }) => {
                  return {
                    id,
                    ...remainAttr,
                    color: sourceId === id ? newColor : color,
                  };
                })
              );
            }
          }}
        />
      </td>
    </tr>
  );
};

export default TableRow;
