import React from "react";

interface TableHeadProps {
  columns: string[];
}

const TableHead: React.FC<TableHeadProps> = (props) => {
  const { columns } = props;
  return (
    <thead className="table-head">
      {columns.map((title: string) => (
        <th key={title}>{title}</th>
      ))}
    </thead>
  );
};

export default TableHead;
