import React from "react";
import { Product } from "../../models/product";
import { Color } from "../../models/color";
import Button from "../Button";
import { IMAGE_PLACE_HOLDER, COLORS } from "../../constants";

interface ModalProps {
  title: string;
  products: Product[];
  colors: Color[];
  setOpen: (isOpen: boolean) => void;
}

const Modal: React.FC<ModalProps> = (props) => {
  const { setOpen, title, products } = props;

  return (
    <div className="modal">
      <div className="modal-content">
        <div className="modal-header">
          <span className="close-button" onClick={() => setOpen(false)}>
            &times;
          </span>
          <p>{title}</p>
        </div>
        <div className="modal-products-showcase">
          {products?.map((product) => {
            const { name, image, color, sku, id } = product;
            const displayImage = image || IMAGE_PLACE_HOLDER;
            return (
              <div className="product-card">
                <div>
                  <img src={displayImage} alt={name} width={"150px"} />
                </div>
                <div className="product-card-detail">
                  <p className="product-card-name">{name}</p>
                  <p>
                    ID: <span>{id}</span>
                  </p>
                  <p>
                    SKU: <span className="product-card-sku">{sku}</span>
                  </p>
                  <p>
                    Color: <span>{color ? COLORS[color] : "White"}</span>
                  </p>
                </div>
              </div>
            );
          })}
        </div>
        <div className="modal-footer">
          <Button
            value="OK"
            className="submit-button"
            onClick={() => setOpen(false)}
          />
        </div>
      </div>
    </div>
  );
};

export default Modal;
