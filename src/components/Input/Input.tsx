import React, { ChangeEvent, HTMLAttributes, useState } from "react";

interface InputProps extends HTMLAttributes<HTMLInputElement> {
  maximumLength?: number;
  isRequired?: boolean;
  className?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.FC<InputProps> = (props) => {
  const {
    maximumLength = Number.POSITIVE_INFINITY,
    isRequired = false,
    className,
    onChange,
    ...remainProps
  } = props;

  const [error, setError] = useState("");

  return (
    <div className="input-component">
      <input
        type="text"
        className={`table-input ${className}`}
        style={{ border: `1px solid ${error ? "red" : "#ddd"}` }}
        {...remainProps}
        onChange={(event) => {
          const value = event.target.value;
          onChange(event);
          if (isRequired && !value.length) {
            setError("This field is required !");
          } else if (maximumLength < value.length) {
            setError(`Only ${maximumLength} character allowed`);
          } else {
            setError("");
          }
        }}
      />
      <div className="error-text">{error}</div>
    </div>
  );
};

export default Input;
