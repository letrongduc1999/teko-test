import React from "react";

interface ErrorsProps {
  errors: string[];
}

const Errors: React.FC<ErrorsProps> = (props) => {
  const { errors } = props;
  return (
    <div className="errors-section">
      {errors.length > 0 && (
        <>
          <p>Warning:</p>
          <ul className="errors-list">
            {errors
              .filter((error) => error.length !== 0)
              .map((error) => (
                <li>{error}</li>
              ))}
          </ul>
        </>
      )}
    </div>
  );
};

export default Errors;
