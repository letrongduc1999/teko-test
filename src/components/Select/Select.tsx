import React, { ChangeEvent, HTMLAttributes } from "react";

type Option = {
  id: number;
  name: string;
};

interface SelectProps extends HTMLAttributes<HTMLSelectElement> {
  defaultValue?: number;
  options?: Option[];
  onChange: (event: ChangeEvent<HTMLSelectElement>) => void;
}

const Select: React.FC<SelectProps> = (props) => {
  const { defaultValue, options, onChange } = props;
  return (
    <select
      className="table-select"
      defaultValue={defaultValue}
      onChange={onChange}
    >
      {!!options?.length &&
        options.map(({ id, name }) => (
          <option key={id} value={id} label={name}>
            {name}
          </option>
        ))}
    </select>
  );
};

export default Select;
