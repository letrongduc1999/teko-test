import { Product } from "../models/product";
import { Color } from "../models/color";

export const getProducts = async (): Promise<Product[]> => {
  const res = await fetch("https://hiring-test.stag.tekoapis.net/api/products");
  return await res.json();
};

export const getColors = async (): Promise<Color[]> => {
  const res = await fetch("https://hiring-test.stag.tekoapis.net/api/colors");
  return await res.json();
};
