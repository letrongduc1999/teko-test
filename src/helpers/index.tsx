import { Product } from "../models/product";
import { NAME_MAX_LENGTH, SKU_MAX_LENGTH } from "../constants";

export const getProductsErrorMessages = (products: Product[]): string[] => {
  if (!products?.length) return [];

  const invalidNameProductIdList = products
    .filter(({ name }) => !name)
    .map((product) => product.id);

  const invalidSkuProductIdList = products
    .filter(({ sku }) => !sku)
    .map((product) => product.id);

  const tooLongNameProductIdList = products
    .filter(({ name }) => name && name.length > NAME_MAX_LENGTH)
    .map((product) => product.id);

  const tooLongSkuProductIdList = products
    .filter(({ sku }) => sku && sku.length > SKU_MAX_LENGTH)
    .map((product) => product.id);

  const invalidNameMsg = !!invalidNameProductIdList.length
    ? `Products with id ${invalidNameProductIdList.join()} have invalid name`
    : "";
  const invalidSkuMsg = !!invalidSkuProductIdList.length
    ? `Products with id ${invalidSkuProductIdList.join()} have invalid sku`
    : "";

  const tooLongNameMsg = !!tooLongNameProductIdList.length
    ? `Products with id ${tooLongNameProductIdList.join()} have over 50 characters in their name`
    : "";
  const tooLongSkuMsg = !!tooLongSkuProductIdList.length
    ? `Products with id ${tooLongSkuProductIdList.join()} have over 20 characters in their sku`
    : "";

  if (invalidNameMsg || invalidSkuMsg) {
    return [invalidNameMsg, tooLongNameMsg, invalidSkuMsg, tooLongSkuMsg];
  }

  return [];
};
